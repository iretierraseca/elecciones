/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package packageElecciones.MODELO;

/**
 *
 * @author Irene
 */
public class ExceptionErrorGeneral extends Exception{
    private int codigoError;
    private String mensajeError;
    private String alertaError;

    public ExceptionErrorGeneral(int codigoError, String mensajeError, String alertaError) {
        this.codigoError = codigoError;
        this.mensajeError = mensajeError;
        this.alertaError = alertaError;
    }

    @Override
    public String toString() {
        return "ApErrorException{" + "codigoError=" + codigoError +" " + mensajeError + " " + alertaError + '}';
    }
}
