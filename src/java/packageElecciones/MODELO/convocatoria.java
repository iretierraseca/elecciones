/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package packageElecciones.MODELO;

import java.time.LocalDate;



/**
 *
 * @author Irene
 */
public class convocatoria {
    private String tipoEleccionConvocatoria;
    private String provinciaConvocatoria;
    private int numeroCargosConvocatoria;
    private LocalDate fechaConsulta;
    private String escrutinio;

    /**
     * 
     * @param tipoEleccionConvocatoria
     * @param provinciaConvocatoria
     * @param numeroCargosConvocatoria
     * @param fechaConsulta
     * @param escrutinio 
     */
    public convocatoria(String tipoEleccionConvocatoria, String provinciaConvocatoria, int numeroCargosConvocatoria, LocalDate fechaConsulta, String escrutinio) {
        this.tipoEleccionConvocatoria = tipoEleccionConvocatoria;
        this.provinciaConvocatoria = provinciaConvocatoria;
        this.numeroCargosConvocatoria = numeroCargosConvocatoria;
        this.fechaConsulta = fechaConsulta;
        this.escrutinio = escrutinio;
    }

    public convocatoria() {
    }
    
    

    public String getTipoEleccionConvocatoria() {
        return tipoEleccionConvocatoria;
    }

    public void setTipoEleccionConvocatoria(String tipoEleccionConvocatoria) {
        this.tipoEleccionConvocatoria = tipoEleccionConvocatoria;
    }

    public String getProvinciaConvocatoria() {
        return provinciaConvocatoria;
    }

    public void setProvinciaConvocatoria(String provinciaConvocatoria) {
        this.provinciaConvocatoria = provinciaConvocatoria;
    }

    public int getNumeroCargosConvocatoria() {
        return numeroCargosConvocatoria;
    }

    public void setNumeroCargosConvocatoria(int numeroCargosConvocatoria) {
        this.numeroCargosConvocatoria = numeroCargosConvocatoria;
    }

    public LocalDate getFechaConsulta() {
        return fechaConsulta;
    }

    public void setFechaConsulta(LocalDate fechaConsulta) {
        this.fechaConsulta = fechaConsulta;
    }

    public String getEscrutinio() {
        return escrutinio;
    }

    public void setEscrutinio(String escrutinio) {
        this.escrutinio = escrutinio;
    }
    
    
    
    
}
