/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package packageElecciones.MODELO;

/**
 *
 * @author Irene
 */
public class partido implements Comparable<partido>{
    private String nombrePartido;
    private String siglasPartido;
    private String logotipoPartido;
    private int numeroVotosPartido;
    private int idPartido;

    /**
     * 
     * @param nombrePartido
     * @param siglasPartido
     * @param logotipoPartido
     * @param numeroVotosPartido 
     */
    public partido(String nombrePartido, String siglasPartido, String logotipoPartido, int numeroVotosPartido) {
        this.nombrePartido = nombrePartido;
        this.siglasPartido = siglasPartido;
        this.logotipoPartido = logotipoPartido;
        this.numeroVotosPartido = numeroVotosPartido;
    }

    /**
     * 
     * @param nombrePartido
     * @param siglasPartido
     * @param logotipoPartido
     * @param numeroVotosPartido
     * @param idPartido 
     */
    public partido(String nombrePartido, String siglasPartido, String logotipoPartido, int numeroVotosPartido, int idPartido) {
        this.nombrePartido = nombrePartido;
        this.siglasPartido = siglasPartido;
        this.logotipoPartido = logotipoPartido;
        this.numeroVotosPartido = numeroVotosPartido;
        this.idPartido = idPartido;
    }
    
  
    public partido(String siglasPartido) {
        this.siglasPartido = siglasPartido;
    }

    public int getIdPartido() {
        return idPartido;
    }

    public void setIdPartido(int idPartido) {
        this.idPartido = idPartido;
    }
    
    public String getNombrePartido() {
        return nombrePartido;
    }

    public void setNombrePartido(String nombrePartido) {
        this.nombrePartido = nombrePartido;
    }

    public String getSiglasPartido() {
        return siglasPartido;
    }

    public void setSiglasPartido(String siglasPartido) {
        this.siglasPartido = siglasPartido;
    }

    public String getLogotipoPartido() {
        return logotipoPartido;
    }

    public void setLogotipoPartido(String logotipoPartido) {
        this.logotipoPartido = logotipoPartido;
    }

    public int getNumeroVotosPartido() {
        return numeroVotosPartido;
    }

    public void setNumeroVotosPartido(int numeroVotosPartido) {
        this.numeroVotosPartido = numeroVotosPartido;
    }
    
    @Override
        public int compareTo(partido o) {
            if (this.numeroVotosPartido > o.numeroVotosPartido) {
                return -1;
            }
            if (this.numeroVotosPartido < o.numeroVotosPartido) {
                return 1;
            }
            return 0;
        }
    
    
    
}
