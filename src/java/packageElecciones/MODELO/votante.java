/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package packageElecciones.MODELO;

import java.time.LocalDate;

/**
 *
 * @author Irene
 */
public class votante {

    String nombreVotante;
    String apellidoVotante;
    String nifVotante;
    String domicilioVotante;
    String passVotante;
    LocalDate fechaNacVotante;
    String votado;
    String rol;

    /**
     * 
     * @param nombreVotante
     * @param apellidoVotante
     * @param nifVotante
     * @param domicilioVotante
     * @param passVotante
     * @param fechaNacVotante
     * @param votado
     * @param rol 
     */
    public votante(String nombreVotante, String apellidoVotante, String nifVotante, String domicilioVotante, String passVotante, LocalDate fechaNacVotante, String votado, String rol) {
        this.nombreVotante = nombreVotante;
        this.apellidoVotante = apellidoVotante;
        this.nifVotante = nifVotante;
        this.domicilioVotante = domicilioVotante;
        this.passVotante = passVotante;
        this.fechaNacVotante = fechaNacVotante;
        this.votado = votado;
        this.rol = rol;
    }

    
    
    /**
     * 
     * @param nombreVotante
     * @param apellidoVotante
     * @param nifVotante
     * @param domicilioVotante
     * @param passVotante
     * @param fechaNacVotante 
     */
    public votante(String nombreVotante, String apellidoVotante, String nifVotante, String domicilioVotante, String passVotante, LocalDate fechaNacVotante) {
        this.nombreVotante = nombreVotante;
        this.apellidoVotante = apellidoVotante;
        this.nifVotante = nifVotante;
        this.domicilioVotante = domicilioVotante;
        this.passVotante = passVotante;
        this.fechaNacVotante = fechaNacVotante;
    }

    /**
     * 
     * @param nombreVotante
     * @param apellidoVotante
     * @param nifVotante
     * @param domicilioVotante
     * @param passVotante 
     */
    public votante(String nombreVotante, String apellidoVotante, String nifVotante, String domicilioVotante, String passVotante) {
        this.nombreVotante = nombreVotante;
        this.apellidoVotante = apellidoVotante;
        this.nifVotante = nifVotante;
        this.domicilioVotante = domicilioVotante;
        this.passVotante = passVotante;
    }

    /**
     * 
     * @param nifVotante
     * @param passVotante 
     */
    public votante(String nifVotante, String passVotante) {
        this.nifVotante = nifVotante;
        this.passVotante = passVotante;
    }

    public String getNombreVotante() {
        return nombreVotante;
    }

    public void setNombreVotante(String nombreVotante) {
        this.nombreVotante = nombreVotante;
    }

    public String getApellidoVotante() {
        return apellidoVotante;
    }

    public void setApellidoVotante(String apellidoVotante) {
        this.apellidoVotante = apellidoVotante;
    }

    public String getNifVotante() {
        return nifVotante;
    }

    public void setNifVotante(String nifVotante) {
        this.nifVotante = nifVotante;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public String getRol() {
        return rol;
    }

    public String getDomicilioVotante() {
        return domicilioVotante;
    }

    public void setDomicilioVotante(String domicilioVotante) {
        this.domicilioVotante = domicilioVotante;
    }

    public String getPassVotante() {
        return passVotante;
    }

    public void setPassVotante(String passVotante) {
        this.passVotante = passVotante;
    }

    public LocalDate getFechaNacVotante() {
        return fechaNacVotante;
    }

    public void setFechaNacVotante(LocalDate fechaNacVotante) {
        this.fechaNacVotante = fechaNacVotante;
    }

    public String getVotado() {
        return votado;
    }

    public void setVotado(String votado) {
        this.votado = votado;
    }

    @Override
    public String toString() {
        return "Nombre: " + this.getNombreVotante() + "<br>"
                + "Apellido: " + this.getApellidoVotante() + "<br>"
                + "Nif: " + this.getNifVotante() + "<br>"
                + "Domicilio: " + this.getDomicilioVotante() + "<br>"
                + "Password: " + this.getPassVotante() + "<br>"
                + "Fecha Nacimiento: " + this.getFechaNacVotante() + "<br>"
                + "Votado: " + this.getVotado() + "<br>"
                + "Rol: " + this.getRol();
    }

}
