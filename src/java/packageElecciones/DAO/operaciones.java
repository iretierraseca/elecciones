/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package packageElecciones.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import packageElecciones.MODELO.ExceptionErrorGeneral;
import packageElecciones.MODELO.convocatoria;
import packageElecciones.MODELO.partido;
import packageElecciones.MODELO.votante;

/**
 *
 * @author Irene
 */
public class operaciones {

    public void registroVotante(votante objVotante, Connection Conexion) throws ExceptionErrorGeneral {

        try {
            //AES_ENCRYPT->ENCRIPTACION PASS VOTANTE
            String ordenSQL = "INSERT INTO votante VALUES (null,?,AES_ENCRYPT(?,'irene'),?,?,?,?,'N','Votante')";
            PreparedStatement PrepSt = Conexion.prepareStatement(ordenSQL);
            //NIF
            PrepSt.setString(1, objVotante.getNifVotante());
            //PASS
            PrepSt.setString(2, objVotante.getPassVotante());
            //FECHA NACIMIENTO
            PrepSt.setDate(3, java.sql.Date.valueOf(objVotante.getFechaNacVotante()));
            //NOMBRE
            PrepSt.setString(4, objVotante.getNombreVotante());
            //APELLIDO
            PrepSt.setString(5, objVotante.getApellidoVotante());
            //DOMICILIO
            PrepSt.setString(6, objVotante.getDomicilioVotante());

            int rs = PrepSt.executeUpdate();

        } catch (SQLException ex) {
            String mensajeError = ex.getMessage(); //Descripción del mensaje de error
            int codigoError = ex.getErrorCode(); //Codigo de error que lanza la base de datos
            String alertaError = "No se ha podido dar de alta al votante";

            throw new ExceptionErrorGeneral(codigoError, mensajeError, alertaError);
        }
    }

    public votante autenticacionVotante(votante objVotante, Connection Conexion) throws ExceptionErrorGeneral {

        try {
            String ordenSQL = "SELECT *, aes_decrypt(passVotante, 'irene') as pass FROM votante WHERE nifVotante=? AND aes_decrypt(passVotante, 'irene')=?";
            PreparedStatement PrepSt = Conexion.prepareStatement(ordenSQL);
            //NIF
            PrepSt.setString(1, objVotante.getNifVotante());
            //PASS
            PrepSt.setString(2, objVotante.getPassVotante());
            ResultSet rs = PrepSt.executeQuery();

            if (rs.next()) {
                String nifVotante = rs.getString("nifVotante");
                String passVotante = rs.getString("pass");
                LocalDate fechaNacVotante = rs.getDate("fechaNacVotante").toLocalDate();
                String nombreVotante = rs.getString("nombreVotante");
                String apellidoVotante = rs.getString("apellidoVotante");
                String domicilioVotante = rs.getString("domicilioVotante");
                String votado = rs.getString("votado");
                String rol = rs.getString("rol");

                votante objetoVotante = new votante(nombreVotante, apellidoVotante, nifVotante, domicilioVotante, passVotante, fechaNacVotante, votado, rol);
                return objetoVotante;
            } else {
                //votante no encontrado, lanzar error
                throw new ExceptionErrorGeneral(404, "Error", "");
            }//comparar cadenas con equals
        } catch (SQLException ex) {
            String mensajeError = ex.getMessage(); //Descripción del mensaje de error
            int codigoError = ex.getErrorCode(); //Codigo de error que lanza la base de datos
            String alertaError = "No se ha podido efectuar la identificacion";
            throw new ExceptionErrorGeneral(codigoError, mensajeError, alertaError);
        }
    }

    public convocatoria informacionConvocatoria(Connection Conexion) throws ExceptionErrorGeneral {

        try {
            String ordenSQL = "SELECT * FROM convocatoria";
            PreparedStatement PrepSt = Conexion.prepareStatement(ordenSQL);

            ResultSet rs = PrepSt.executeQuery();

            if (rs.next()) {
                String tipoEleccionConvocatoria = rs.getString("tipoEleccionConvocatoria");
                String provinciaConvocatoria = rs.getString("provinciaConvocatoria");
                int numeroCargosConvocatoria = rs.getInt("numeroCargosConvocatoria");
                LocalDate fechaConsulta = rs.getDate("fechaConsulta").toLocalDate();
                String escrutinio = rs.getString("escrutinio");

                convocatoria objetoConvocatoria = new convocatoria(tipoEleccionConvocatoria, provinciaConvocatoria, numeroCargosConvocatoria, fechaConsulta, escrutinio);
                return objetoConvocatoria;
            } else {
                //convocatoriano encontrada
                throw new ExceptionErrorGeneral(404, "Error", "");
            }//comparar cadenas con equals
        } catch (SQLException ex) {
            String mensajeError = ex.getMessage(); //Descripción del mensaje de error
            int codigoError = ex.getErrorCode(); //Codigo de error que lanza la base de datos
            String alertaError = "No se ha podido obtener la información de la convocatoria";
            throw new ExceptionErrorGeneral(codigoError, mensajeError, alertaError);
        }

    }

    public ArrayList<votante> listadoCensoAdministrador(Connection Conexion) throws ExceptionErrorGeneral {

        ArrayList<votante> arrayVotantes = new ArrayList<votante>();

        try {
            String ordenSQL = "SELECT * FROM votante";
            PreparedStatement PrepSt = Conexion.prepareStatement(ordenSQL);
            ResultSet rs = PrepSt.executeQuery();

            while (rs.next()) {

                String nifVotante = rs.getString("nifVotante");
                String passVotante = rs.getString("passVotante");
                LocalDate fechaNacVotante = LocalDate.parse(rs.getString("fechaNacVotante"));
                String nombreVotante = rs.getString("nombreVotante");
                String apellidoVotante = rs.getString("apellidoVotante");
                String domicilioVotante = rs.getString("domicilioVotante");
                String votado = rs.getString("votado");
                String rol = rs.getString("rol");

                votante objetoVotante = new votante(nombreVotante, apellidoVotante, nifVotante, domicilioVotante, passVotante, fechaNacVotante, votado, rol);

                arrayVotantes.add(objetoVotante);

            }

        } catch (SQLException ex) {
            String mensajeError = ex.getMessage(); //Descripción del mensaje de error
            int codigoError = ex.getErrorCode(); //Codigo de error que lanza la base de datos
            String alertaError = "No se ha podido efectuar el listado";
            throw new ExceptionErrorGeneral(codigoError, mensajeError, alertaError);
        }

        return arrayVotantes;

    }

    public void editarPerfilVotante(votante objVotante, Connection Conexion) throws ExceptionErrorGeneral {

        try {
            String ordenSQL = "UPDATE votante SET passVotante=AES_ENCRYPT(?,'irene'), fechaNacVotante=?, nombreVotante=?, apellidoVotante=?, domicilioVotante=?"
                    + "WHERE nifVotante=?";
            PreparedStatement PrepSt = Conexion.prepareStatement(ordenSQL);

            //PASS
            PrepSt.setString(1, objVotante.getPassVotante());
            //FECHA NACIMIENTO
            PrepSt.setDate(2, java.sql.Date.valueOf(objVotante.getFechaNacVotante()));
            //NOMBRE
            PrepSt.setString(3, objVotante.getNombreVotante());
            //APELLIDO
            PrepSt.setString(4, objVotante.getApellidoVotante());
            //DOMICILIO
            PrepSt.setString(5, objVotante.getDomicilioVotante());
            //NIF
            PrepSt.setString(6, objVotante.getNifVotante());

            int rs = PrepSt.executeUpdate();

            if (rs != 1) {
                throw new ExceptionErrorGeneral(406, "Error", "no se ha podido editar el perfil");
            }

        } catch (SQLException ex) {
            String mensajeError = ex.getMessage(); //Descripción del mensaje de error
            int codigoError = ex.getErrorCode(); //Codigo de error que lanza la base de datos
            String alertaError = "No se ha podido dar de alta al votante";

            throw new ExceptionErrorGeneral(codigoError, mensajeError, alertaError);
        }
    }

    public void bajaVotante(votante objVotante, Connection Conexion) throws ExceptionErrorGeneral {
        try {
            String ordenSQL = "DELETE FROM votante WHERE nifVotante=? AND votado='N'";
            PreparedStatement PrepSt = Conexion.prepareStatement(ordenSQL);
            //NIF
            PrepSt.setString(1, objVotante.getNifVotante());

            int rs = PrepSt.executeUpdate();

            if (rs == 0) {
                throw new SQLException("Error", " no se ha podido efectuar la baja", 400);
            }
        } catch (SQLException ex) {
            String mensajeError = ex.getMessage(); //Descripción del mensaje de error
            int codigoError = ex.getErrorCode(); //Codigo de error que lanza la base de datos
            String alertaError = "No se ha podido efectuar la baja";

            throw new ExceptionErrorGeneral(codigoError, mensajeError, alertaError);
        }
    }

    public ArrayList<partido> listadoPartido(Connection Conexion) throws ExceptionErrorGeneral {

        ArrayList<partido> arrayPartidos = new ArrayList<>();

        try {
            String ordenSQL = "SELECT * FROM partido";
            PreparedStatement PrepSt = Conexion.prepareStatement(ordenSQL);
            ResultSet rs = PrepSt.executeQuery();

            while (rs.next()) {

                String nombrePartido = rs.getString("nombrePartido");
                String siglasPartido = rs.getString("siglasPartido");
                String logotipoPartido = rs.getString("logotipoPartido");
                int numeroVotosPartido = rs.getInt("numeroVotosPartido");

                partido objetoPartido = new partido(nombrePartido, siglasPartido, logotipoPartido, numeroVotosPartido);

                arrayPartidos.add(objetoPartido);
            }
        } catch (SQLException ex) {
            String mensajeError = ex.getMessage(); //Descripción del mensaje de error
            int codigoError = ex.getErrorCode(); //Codigo de error que lanza la base de datos
            String alertaError = "No se han podido obtener los partidos";
            throw new ExceptionErrorGeneral(codigoError, mensajeError, alertaError);
        }

        return arrayPartidos;

    }

    public void votarPartido(Connection Conexion, partido _objPartido, votante _objVotante) throws ExceptionErrorGeneral {

        try {

            String ordenSQL = "UPDATE partido SET numeroVotosPartido = numeroVotosPartido + 1 WHERE siglasPartido=?";
            PreparedStatement PrepSt = Conexion.prepareStatement(ordenSQL);
            PrepSt.setString(1, _objPartido.getSiglasPartido());
            int rsActualizarVotos = PrepSt.executeUpdate();

            if (rsActualizarVotos != 1) {
                throw new ExceptionErrorGeneral(2005, "Error", "no se ha podido actualizar el número de votos");
            }

            String ordenSQLVotado = "UPDATE votante SET votado = 'Y' WHERE nifVotante=?";
            PreparedStatement PrepStVotado = Conexion.prepareStatement(ordenSQLVotado);
            PrepStVotado.setString(1, _objVotante.getNifVotante());
            int rsActualizarVotoVotante = PrepStVotado.executeUpdate();

            if (rsActualizarVotoVotante != 1) {
                throw new ExceptionErrorGeneral(2005, "Error", "no se ha podido actualizar los datos del votante");
            }

        } catch (SQLException ex) {
            String mensajeError = ex.getMessage(); //Descripción del mensaje de error
            int codigoError = ex.getErrorCode(); //Codigo de error que lanza la base de datos
            String alertaError = "Error";
            throw new ExceptionErrorGeneral(codigoError, mensajeError, alertaError);
        }

    }

    public void escrutinioAdministrador(Connection Conexion, String _opcionEscrutinio) throws ExceptionErrorGeneral {

        try {
            String ordenSQL = "UPDATE convocatoria SET escrutinio=?";
            PreparedStatement PrepSt = Conexion.prepareStatement(ordenSQL);

            //OPCION ESCRUTINIO {abierto, finalizado}
            PrepSt.setString(1, _opcionEscrutinio);
            int rs = PrepSt.executeUpdate();

            if (rs != 1) {
                throw new ExceptionErrorGeneral(406, "Error", "no se ha podido editar la información de la convocatoria");
            }

        } catch (SQLException ex) {
            String mensajeError = ex.getMessage(); //Descripción del mensaje de error
            int codigoError = ex.getErrorCode(); //Codigo de error que lanza la base de datos
            String alertaError = "No se ha podido realizar la actualización de datos";

            throw new ExceptionErrorGeneral(codigoError, mensajeError, alertaError);
        }
    }

    /**
     *
     * @param esc escaños a repartir
     * @param Conexion
     */
    public ArrayList pruebaAdministrador(Connection Conexion) throws ExceptionErrorGeneral {

        int esc = 4;//Escaños

        ArrayList<ArrayList<Float>> arrayListNumeros = new ArrayList<ArrayList<Float>>();
        //[[89564, 44782, 14927, 3731], [59461, 29730, 9910, 2477], [33301, 16650, 5550, 1387], [31883, 15941, 5313, 1328]]
        ArrayList<Float> Totnumeros = new ArrayList<Float>();
        ArrayList<Float> votosEscs = new ArrayList<Float>();
        ArrayList<Float> arrayx = new ArrayList<Float>();
        ArrayList<Integer> arrayy = new ArrayList<Integer>();
        ArrayList<String> partido = new ArrayList<String>();
        ArrayList partidoEsc = new ArrayList<>();

        try {
            String ordenSQL = "SELECT * FROM partido WHERE nombrePartido != 'VOTO EN BLANCO' AND nombrePartido!='VOTO NULO'";
            PreparedStatement PrepSt = Conexion.prepareStatement(ordenSQL);
            ResultSet rs = PrepSt.executeQuery();
            int numeroPartidos = 0;
            while (rs.next()) {
                numeroPartidos++;
                float numeroVotos = rs.getInt("numeroVotosPartido");
                String nombrePartido = rs.getString("nombrePartido");
                partido.add(nombrePartido);
                ArrayList<Float> numeros = new ArrayList<Float>();
                for (int i = 1; i < esc + 1; i++) {
                    numeros.add(numeroVotos / i);
                    Totnumeros.add(numeroVotos / i);
                }
                arrayListNumeros.add(numeros);
            }

            Comparator<Float> comparador = Collections.reverseOrder();
            Collections.sort(Totnumeros, comparador);

            for (int x = 0; x < esc; x++) {
                votosEscs.add(Totnumeros.get(x));
            }

            //comparar arrayListNumeros con votosEscs
            int contador = 0;
            //r numero de partidos que superan los votos exigidos para obtener escaños
            for (int r = 0; r < numeroPartidos; r++) {
                contador = 0;
                for (int x = 0; x < votosEscs.size(); x++) {
                    float numero1 = votosEscs.get(x);
                    for (int y = 0; y < esc; y++) {
                        arrayx = arrayListNumeros.get(r);
                        float numero2 = arrayx.get(y);
                        if (numero1 == numero2) {
                            contador++;
                        }
                    }
                }
                arrayy.add(contador);
            }

            //arrayListNumeros
            //[[89564, 44782, 14927, 3731], [59461, 29730, 9910, 2477], [33301, 16650, 5550, 1387], [31883, 15941, 5313, 1328]]
            //votosEscs
            //[89564, 59461, 44782, 33301]
        } catch (SQLException ex) {
            String mensajeError = ex.getMessage(); //Descripción del mensaje de error
            int codigoError = ex.getErrorCode(); //Codigo de error que lanza la base de datos
            String alertaError = "No se ha podido efectuar el listado";
            throw new ExceptionErrorGeneral(codigoError, mensajeError, alertaError);
        }

        partidoEsc.add(partido);
        partidoEsc.add(arrayy);
        return partidoEsc;
    }

    public ArrayList pruebaAdministradorV2(Connection Conexion) throws ExceptionErrorGeneral {

        int esc = 4;//Escaños

        ArrayList<partido> arrayPartidos = new ArrayList<>();

        try {
            String ordenSQL = "SELECT * FROM partido WHERE nombrePartido != 'VOTO EN BLANCO' AND nombrePartido!='VOTO NULO'";
            PreparedStatement PrepSt = Conexion.prepareStatement(ordenSQL);
            ResultSet rs = PrepSt.executeQuery();
            int numeroPartidos = 0;
            while (rs.next()) {
                int idPartido = rs.getInt("idPartido");
                String nombrePartido = rs.getString("nombrePartido");
                String siglasPartido = rs.getString("siglasPartido");
                String logotipoPartido = rs.getString("logotipoPartido");
                int numeroVotosPartido = rs.getInt("numeroVotosPartido");

                partido objetoPartido = new partido(nombrePartido, siglasPartido, logotipoPartido, numeroVotosPartido, idPartido);

                arrayPartidos.add(objetoPartido);
            }

            Collections.sort(arrayPartidos);

            for (int i = 0, contador=0; i < 4; i++) {
                Collections.sort(arrayPartidos);
                
           
                String ordenSQLP = "SELECT numeroVotosPartido AS votos FROM partido WHERE idPartido=?";
                PreparedStatement PrepStP = Conexion.prepareStatement(ordenSQLP);
                PrepStP.setInt(1, arrayPartidos.get(0).getIdPartido());
                ResultSet rsP = PrepStP.executeQuery();
                rsP.next();
                int votos = rsP.getInt("votos");
                
                
                String ordenSQLC = "SELECT idCandidato AS candidato FROM candidato WHERE fkIdPartidoCandidato=? AND ordenEleccionCandidato=?";
                PreparedStatement PrepStC = Conexion.prepareStatement(ordenSQLC);
                PrepStC.setInt(1, arrayPartidos.get(0).getIdPartido());
                PrepStC.setInt(2, contador+1);
                ResultSet rsC = PrepStC.executeQuery();
                rsC.next();
                
                int idCandidato = rsC.getInt("candidato");
                
                String ordenSQLesc = "INSERT INTO escaño VALUES (null,?,?,?)";
                PreparedStatement PrepStEsc = Conexion.prepareStatement(ordenSQLesc);
                PrepStEsc.setInt(1, arrayPartidos.get(0).getIdPartido());
                PrepStEsc.setInt(2, 1);
                PrepStEsc.setInt(3, idCandidato);
                int rsEsc = PrepStEsc.executeUpdate();
                
                String ordenSQLE = "SELECT COUNT(*) AS total FROM escaño WHERE idFkPartido=?";
                PreparedStatement PrepStE = Conexion.prepareStatement(ordenSQLE);
                PrepStE.setInt(1, arrayPartidos.get(0).getIdPartido());
                ResultSet rsE = PrepStE.executeQuery();
                rsE.next();
                contador = rsE.getInt("total")+1;
               
                arrayPartidos.get(0).setNumeroVotosPartido( votos/contador);
                contador--;
            }

        } catch (SQLException ex) {
        }

        return arrayPartidos;

    }
    /*
    String ordenSQL = "INSERT INTO votante VALUES (null,?,AES_ENCRYPT(?,'irene'),?,?,?,?,'N','Votante')";
            PreparedStatement PrepSt = Conexion.prepareStatement(ordenSQL);
            //NIF
            PrepSt.setString(1, objVotante.getNifVotante());
            //PASS
            PrepSt.setString(2, objVotante.getPassVotante());
            //FECHA NACIMIENTO
            PrepSt.setDate(3, java.sql.Date.valueOf(objVotante.getFechaNacVotante()));
            //NOMBRE
            PrepSt.setString(4, objVotante.getNombreVotante());
            //APELLIDO
            PrepSt.setString(5, objVotante.getApellidoVotante());
            //DOMICILIO
            PrepSt.setString(6, objVotante.getDomicilioVotante());

            int rs = PrepSt.executeUpdate();
     */

}
