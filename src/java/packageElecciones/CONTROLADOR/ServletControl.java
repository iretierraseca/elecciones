/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package packageElecciones.CONTROLADOR;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import packageElecciones.MODELO.ExceptionErrorGeneral;
import packageElecciones.MODELO.convocatoria;
import packageElecciones.MODELO.votante;

/**
 *
 * @author Irene
 */
public class ServletControl extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String Peticion = request.getParameter("opcion");

            HttpSession session = request.getSession(true);
            votante objVotante = (votante) session.getAttribute("usuario");
            convocatoria objConvocatoria = (convocatoria) session.getAttribute("convocatoria");

            String parametro = "";
            //Comprobar si el usuario es administrador o votante para el menu en las vistas de error y exito
            if (objVotante.getRol().equals("Votante")) {
                parametro = "vistaMenuVotante";
            } else if (objVotante.getRol().equals("Administrador")) {
                parametro = "vistaMenuAdministrador";
            }

            try {
                switch (Peticion) {
                    case "Votar":
                        if (objConvocatoria.getEscrutinio().equals("Finalizado")) {
                            throw new ExceptionErrorGeneral(2004, "Error", "El escrutinio ya ha finalizado");
                        } else if (objVotante.getVotado().equals("Y")) {
                            throw new ExceptionErrorGeneral(2001, "Error", "Ya has votado vez");
                        } else if (objConvocatoria.getEscrutinio().equals("Cerrado")) {
                            throw new ExceptionErrorGeneral(2004, "Error", "El escrutinio esta cerrado");
                        } else {
                            response.sendRedirect("ServletVotar");
                        }
                        break;
                    case "Baja":
                        if (objConvocatoria.getEscrutinio().equals("Finalizado")) {
                            throw new ExceptionErrorGeneral(2004, "Error", "El escrutinio ya ha finalizado");
                        } else if (objVotante.getVotado().equals("Y")) {
                            throw new ExceptionErrorGeneral(2002, "Error", "No puedes darte de baja.Ya has votado");
                        } else {
                            response.sendRedirect("VISTA/vistaVotanteBaja.jsp");
                        }
                        break;
                    case "EditarPerfil":
                        if (objConvocatoria.getEscrutinio().equals("Finalizado")) {
                            throw new ExceptionErrorGeneral(2004, "Error", "El escrutinio ya ha finalizado");
                        } else if (objVotante.getVotado().equals("Y")) {
                            throw new ExceptionErrorGeneral(2003, "Error", "No puedes modificar tu perfil.Ya has votado");
                        } else {
                            response.sendRedirect("VISTA/vistaVotanteEditarPerfil.jsp");
                        }
                        break;
                    case "CerrarSesion":
                        //Cerrar sesion
                        session.setAttribute("usuario", "");
                        session.setAttribute("arrayPartidos", "");
                        session.setAttribute("convocatoria", "");
                        session.invalidate();
                        response.sendRedirect("VISTA/vistaLoginRegistro.jsp");
                        break;
                    case "AbrirEscrutinio":
                        if (objConvocatoria.getEscrutinio().equals("Abierto")) {
                            throw new ExceptionErrorGeneral(2006, "Error", "El proceso electoral ya esta iniciado");
                        } else if (objConvocatoria.getEscrutinio().equals("Finalizado")) {
                            throw new ExceptionErrorGeneral(2006, "Error", "El proceso electoral ya ha finalizado");
                        } else if (objConvocatoria.getEscrutinio().equals("Cerrado")) {
                            String mensaje = "Dar comienzo al proceso electoral";
                            response.sendRedirect("VISTA/vistaAdministradorEscrutinio.jsp?mensajeEscrutinio=" + mensaje + "&opcionEscrutinio=Abierto");
                        }
                        break;
                    case "CerrarEscrutinio":
                        if (objConvocatoria.getEscrutinio().equals("Abierto")) {
                            String mensaje = "Finalizar el proceso electoral";
                            response.sendRedirect("VISTA/vistaAdministradorEscrutinio.jsp?mensajeEscrutinio=" + mensaje + "&opcionEscrutinio=Finalizado");
                        } else if (objConvocatoria.getEscrutinio().equals("Finalizado")) {
                            throw new ExceptionErrorGeneral(2006, "Error", "El proceso electoral ya ha finalizado");
                        } else if (objConvocatoria.getEscrutinio().equals("Cerrado")) {
                            throw new ExceptionErrorGeneral(2006, "Error", "El proceso electoral esta cerrado");
                        }
                        break;
                }

            } catch (ExceptionErrorGeneral ex) {
                response.sendRedirect("VISTA/vistaMensajeError.jsp?tipoUsuario=" + parametro + "&mensajeError=" + ex);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
