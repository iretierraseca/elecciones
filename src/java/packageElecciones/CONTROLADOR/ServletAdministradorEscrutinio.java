/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package packageElecciones.CONTROLADOR;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import packageElecciones.DAO.conexionBD;
import packageElecciones.DAO.operaciones;
import packageElecciones.MODELO.ExceptionErrorGeneral;
import packageElecciones.MODELO.convocatoria;
import packageElecciones.MODELO.votante;

/**
 *
 * @author Irene
 */
public class ServletAdministradorEscrutinio extends HttpServlet {

    private Connection Conexion;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String opcion = request.getParameter("botonBaja");
            HttpSession session = request.getSession(true);
            votante objVotante = (votante) session.getAttribute("usuario");
            convocatoria objConvocatoria = (convocatoria) session.getAttribute("convocatoria");
            String parametro = "";
            if (objVotante.getRol().equals("Votante")) {
                parametro = "vistaMenuVotante";
            } else if (objVotante.getRol().equals("Administrador")) {
                parametro = "vistaMenuAdministrador";
            }

            if (opcion.equals("Aceptar")) {
                String opcionEscrutinio = request.getParameter("opcionEscrutinio");
                try {
                    new operaciones().escrutinioAdministrador(Conexion, opcionEscrutinio);
                    objConvocatoria.setEscrutinio(opcionEscrutinio);
                    //response.sendRedirect("VISTA/vistaLoginRegistro.jsp");
                    //out.println("hecho");
                    response.sendRedirect("VISTA/vistaMensajeExito.jsp?tipoUsuario=" + parametro);
                } catch (ExceptionErrorGeneral ex) {
                    response.sendRedirect("VISTA/vistaMensajeError.jsp?tipoUsuario=" + parametro + "&mensajeError=" + ex);
                }
            } else {
                response.sendRedirect("VISTA/vistaMenuVotante.jsp");
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    @Override
    public void init() throws ServletException {
        super.init();
        /* Establecemos la conexión, si no existe */
        try {
            conexionBD ConexBD = conexionBD.GetConexion();
            Conexion = ConexBD.GetCon();
        } catch (ClassNotFoundException | SQLException cnfe) {
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
