/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package packageElecciones.CONTROLADOR;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDate;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import packageElecciones.DAO.conexionBD;
import packageElecciones.DAO.operaciones;
import packageElecciones.MODELO.ExceptionErrorGeneral;
import packageElecciones.MODELO.convocatoria;
import packageElecciones.MODELO.votante;

/**
 *
 * @author Irene
 */
public class ServletVotanteLogin extends HttpServlet {
     private Connection Conexion;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            
            String nifVotante= request.getParameter("nifLoginVotante"); 
            String passVotante = request.getParameter("passLoginVotante");
    
            votante objVotante = new votante( nifVotante, passVotante);
            try{
                objVotante = new operaciones().autenticacionVotante(objVotante, Conexion);
                //funcion obtener parametros de la aplicacion
                convocatoria objConvocatoria = new operaciones().informacionConvocatoria(Conexion);
                //INICIAR SESIÓN
                HttpSession session = request.getSession(true);
                session.setAttribute("usuario", objVotante);
                session.setAttribute("convocatoria", objConvocatoria);
                
                
                if(objVotante.getRol().equals("Votante")){
                    out.println("menu votante");
                    response.sendRedirect("VISTA/vistaMenuVotante.jsp");
                }else if(objVotante.getRol().equals("Administrador")){
                    out.println("menu administrador");
                    response.sendRedirect("VISTA/vistaMenuAdministrador.jsp");
                }
            }catch(ExceptionErrorGeneral e){
                response.sendRedirect("VISTA/vistaMensajeAdvertencia.jsp?mensajeError="+e);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    @Override
    public void init() throws ServletException {
        super.init();
        /* Establecemos la conexión, si no existe */
        try {
            conexionBD ConexBD = conexionBD.GetConexion();
            Conexion = ConexBD.GetCon();
        } catch (ClassNotFoundException | SQLException cnfe) {
        }
    }
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
