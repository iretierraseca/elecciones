<%-- 
    Document   : vistaVotar
    Created on : 05-dic-2018, 12:13:52
    Author     : Irene
--%>

<%@page import="packageElecciones.MODELO.partido"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%String usuario = request.getParameter("tipoUsuario");%>
<jsp:include page="<%=usuario + ".jsp"%>"/>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <style>
            img{
                height: 40px;
                width: 40px;
            }
            table{
                text-align: center;
            }
        </style>
    </head>
    <body>
        <%
            //HttpSession SesionUsuario = request.getSession(true);
            ArrayList<partido> arrayListPartido = (ArrayList<partido>) session.getAttribute("arrayPartidos");
        %>
        <section id="seccionContainer">
            <div class="container" id="ContainerSeccionError">
                <form action="../ServletVotarPartido">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">Nombre partido</th>
                                <th scope="col">Siglas</th>
                                <th scope="col">Logotipo</th>
                                <th scope="col">Voto</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%
                                for (int i = 0; i < arrayListPartido.size(); i++) {
                            %>
                            <tr>
                                <td><%=arrayListPartido.get(i).getNombrePartido()%></td>
                                <td><%=arrayListPartido.get(i).getSiglasPartido()%></td>
                                <td><img src="<%=arrayListPartido.get(i).getLogotipoPartido()%>"></td>
                                <td><input type="radio" name="partidos" value="<%=arrayListPartido.get(i).getSiglasPartido()%>"></td>
                            </tr>
                            <%
                                }
                            %>

                            <tr>
                                <th colspan="4"><button type="submit" class="btn btn-dark">Votar</button></th>
                            </tr>
                        </tbody>
                    </table>
                </form>
            </div>
        </section>
    </body>
</html>
