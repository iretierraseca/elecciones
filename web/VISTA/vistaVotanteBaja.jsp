<%-- 
    Document   : vistaVotanteBaja
    Created on : 24-nov-2018, 11:50:51
    Author     : Irene
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="vistaMenuVotante.jsp"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <style>
            #containerBaja{
                border: 2px solid #ffeeba !important;
                box-shadow: 2px 2px 5px #ffeeba !important;
            }
        </style>
    </head>
    <body>

        <section id="seccionContainer">
            <div class="container" id="containerBaja">
                <form action="../ServletVotanteBaja">
                    <div class="alert alert-warning mt-3" role="alert">
                        <h4 class="alert-heading">Hola, <%=objVotante.getNombreVotante()%></h4>
                        <p>¿Seguro que quieres darte de baja en el proceso electoral?</p>
                        <hr>
                        <p class="mb-0">
                            <input class="btn btn-primary " type="submit" id="botonEnviarBajaVotante" name="botonBaja" value="Aceptar">
                            <input class="btn btn-warning " type="submit" id="botonEnviarBajaVotante" name="botonBaja" value="Cancelar">
                        </p>
                    </div>
                </form>
            </div>
        </section>
    </body>
</html>
