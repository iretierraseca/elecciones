<%-- 
    Document   : vistaMenuAdministrador
    Created on : 22-nov-2018, 20:19:20
    Author     : Irene
--%>

<%@page import="packageElecciones.MODELO.votante"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="stylesheet" href="../CSS/style.css">
    </head>
    <body>
        <%
            //HttpSession SesionUsuario = request.getSession(true);
            votante objVotante = (votante) session.getAttribute("usuario");

        %>
        <!-- START NAV -->
        <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
            <div class="container">
                <a class="navbar-brand" href="#">
                    <img src="../CSS/IMG/voto.png" width="42" height="42" class="d-inline-block align-top" alt="">
                    <h3 class="d-inline-block " id="icono">Elecciones. <span><%out.print(objVotante.getRol());%></span></h3>
                </a>
                <button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03"
                        aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
                    <ul class="navbar-nav ml-auto  mt-lg-0 ">
                        <li class="nav-item p-1">
                            <!--<a class="nav-link link-ejercicios" href="#">Votar</a>-->
                            <a class="nav-link link-ejercicios" href="../ServletControl?opcion=Votar">Votar</a>
                        </li>
                        <li class="nav-item p-1">
                            <a class="nav-link link-ejercicios" href="../ServletAdministradorListadoCenso" id="ejercicio1.1">Censo</a>
                        </li>
                        <li class="nav-item dropdown p-1">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Escrutinio
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="../ServletControl?opcion=AbrirEscrutinio">Apertura</a>
                                <a class="dropdown-item" href="../ServletControl?opcion=CerrarEscrutinio">Cierre</a>
                            </div>
                        </li>
                        <li class="nav-item p-1">
                            <a class="nav-link link-ejercicios" href="#" id="ejercicio1.1">Resultados</a>
                        </li>
                        <li class="nav-item p-1">
                            <a class="nav-link link-ejercicios" href="../ServletAdministradorPrueba" id="ejercicio1.1">Prueba</a>
                        </li>
                        <li class="nav-item p-1">
                            <!--<a class="nav-link link-ejercicios" href="#">Votar</a>-->
                            <a class="nav-link link-ejercicios" href="../ServletControl?opcion=CerrarSesion">CerrarSesion</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- END NAV -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>
    </body>
</html>