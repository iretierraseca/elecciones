<%-- 
    Document   : vistaMenuVotante
    Created on : 22-nov-2018, 19:44:23
    Author     : Irene
--%>

<%@page import="packageElecciones.MODELO.votante"%>
<%@page import="javax.servlet.http.HttpSession"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="stylesheet" href="../CSS/style.css">
    </head>
    <body>
            <!-- START NAV -->
            <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
                <div class="container">
                    <a class="navbar-brand" href="#">
                        <img src="../CSS/IMG/voto.png" width="42" height="42" class="d-inline-block align-top" alt="">
                        <h1 class="d-inline-block align-top" id="icono">Elecciones. <span>
                            <%
                             //HttpSession SesionUsuario = request.getSession(true);
                             votante objVotante = (votante)session.getAttribute("usuario");
                             out.print(objVotante.getNombreVotante());
                             out.print(objVotante.getRol());
                            %>
                            
                            </span></h1>
                    </a>
                    <button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03"
                            aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
                        <ul class="navbar-nav ml-auto mt-2 mt-lg-0 ">
                            <li class="nav-item p-1">
                                <!--<a class="nav-link link-ejercicios" href="#">Votar</a>-->
                                <a class="nav-link link-ejercicios" href="../ServletControl?opcion=Votar">Votar</a>
                            </li>
                            <li class="nav-item p-1">
                                <!--<a class="nav-link link-ejercicios" href="vistaVotanteBaja.jsp" id="ejercicio1.1">Baja</a>-->
                                <a class="nav-link link-ejercicios" href="../ServletControl?opcion=Baja" id="ejercicio1.1">Baja</a>
                            </li>
                            <li class="nav-item p-1">
                                <!--<a class="nav-link link-ejercicios" href="vistaVotanteEditarPerfil.jsp" id="ejercicio1.1">Editar perfil</a>-->
                                <a class="nav-link link-ejercicios" href="../ServletControl?opcion=EditarPerfil" id="ejercicio1.1">Editar perfil</a>
                            </li>
                            <li class="nav-item p-1">
                                <a class="nav-link link-ejercicios" href="../ServletResultadoVotaciones" id="ejercicio1.1">Resultados votacion</a>
                            </li>
                            <li class="nav-item p-1">
                                <!--<a class="nav-link link-ejercicios" href="vistaVotanteEditarPerfil.jsp" id="ejercicio1.1">Editar perfil</a>-->
                                <a class="nav-link link-ejercicios" href="../ServletControl?opcion=CerrarSesion" id="ejercicio1.1">Cerrar sesion</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <!-- END NAV -->
    </body>
</html>
