<%-- 
    Document   : vistaLoginRegistro
    Created on : 22-nov-2018, 16:54:31
    Author     : Irene
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="stylesheet" href="../CSS/style.css">
        <style>
            @-webkit-keyframes fadeIn {
                from { opacity: 0; }
                to { opacity: 1; }
            }  
            @keyframes fadeIn {
                from { opacity: 0; }
                to { opacity: 1; }
            }
        </style>
    </head>

    <body>
        <section id="seccionContainer" >
            <div class="container rounded mt-5 pl-0 pr-0 opaco">
                <div class="register-photo">
                    <div class="form-container ml-0">
                        <div class="image-holder"></div>
                        <form  action="../ServletVotanteLogin" id="formLogin">
                            <h2 class="text-center mb-3">
                                <!--<img src="../CSS/IMG/voto.png" width="40" height="40" class="d-inline-block align-top" alt="">-->
                                <strong>Login</strong>votante.
                            </h2>
                            <!--NIF-->
                            <div class="form-group row mb-3">
                                <label for="inputNifLogin" class="col-sm-4 col-form-label">NIF</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control " id="inputNifLogin" name="nifLoginVotante"  placeholder="NIF" required>
                                </div>
                            </div>
                            <!--PASSWORD-->
                            <div class="form-group row mb-3">
                                <label for="inputPassLogin" class="col-sm-4 col-form-label">Password</label>
                                <div class="col-sm-8">
                                    <input type="password" class="form-control" id="inputPassLogin" name="passLoginVotante" placeholder="Password" required>
                                </div>
                            </div>
                            <input class="btn btn-outline-secondary mt-1" type="submit" id="botonEnviarBajaVotante" value="Login">
                            <p><a href="#" onclick="login()">REGISTRO</a></p>
                        </form>
                        <form action="../ServletVotanteRegistro" id="formRegistro" style="display:none;">
                            <h2 class="text-center mb-3">
                                <!--<img src="../CSS/IMG/voto.png" width="40" height="40" class="d-inline-block align-top" alt="">-->
                                <strong>Registro</strong>votante.
                            </h2>
                            <!-- NOMBRE, APELLIDOS -->
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="inputNombreVotante">Nombre</label>
                                    <input type="text" class="form-control " id="inputNombreVotante" name="nombreVotante" placeholder="Nombre" required>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputApellidoVotante">Apellidos</label>
                                    <input type="text" class="form-control " id="inputApellidoVotante" name="apellidoVotante" placeholder="Apellido" required>
                                </div>
                            </div>
                            <!--NIF-->
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="inputNifVotante">Nif</label>
                                    <input type="text" class="form-control " id="inputNifVotante" name="nifVotante" placeholder="Nif" required>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputDomicilioVotante">Domicilio</label>
                                    <input type="text" class="form-control " id="inputDomicilioVotante" name="domicilioVotante" placeholder="Domicilio" required>
                                </div>
                            </div>
                            <!--PASSWORD-->
                            <div class="form-group row">
                                <label for="inputPassVotante" class="col-sm-4 col-form-label">Password</label>
                                <div class="col-sm-8">
                                    <input type="password" class="form-control" id="inputPassVotante" name="passVotante" placeholder="Password" required>
                                </div>
                            </div>
                            <!--CONTENIDO-->
                            <div class="form-group row">
                                <label for="inputFechaNacVotante" class="col-sm-4 col-form-label">Fecha Nacimiento</label>
                                <div class="col-sm-8">
                                    <input type="date" class="form-control " id="inputFechaNacVotante" name="fechaNacVotante" placeholder="Fecha" required>
                                </div>
                            </div>

                            <input class="btn btn-outline-secondary " type="submit" id="botonEnviarBajaVotante" value="Registro">
                            <p><a href="#" onclick="registro()">LOGIN</a></p>
                        </form>
                        
                    </div>
                </div>
            </div>
        </section>
        <script>
            
            window.onload=function(){
                registro();
            }
            
            function registro() {
                var formRegistro = document.getElementById("formRegistro");
                formRegistro.style.display = "none";
                var formLogin = document.getElementById("formLogin");
                formLogin.style.display = "block";
                formLogin.style.animation = "fadeIn 2s";
            }
            function login() {
                
                var formRegistro = document.getElementById("formRegistro");
                formRegistro.style.display = "block";
                formRegistro.style.animation = "fadeIn 2s";
                var formLogin = document.getElementById("formLogin");
                formLogin.style.display = "none";
            }
        </script>
    </body>
</html>