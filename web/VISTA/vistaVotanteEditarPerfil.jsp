<%-- 
    Document   : vistaVotanteEditarPerfil
    Created on : 23-nov-2018, 9:18:55
    Author     : Irene
--%>

<%@page import="java.util.Date"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="vistaMenuVotante.jsp"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <section id="seccionContainer" >
            <div class="container rounded mt-5 pl-0 pr-0 opaco">
                <div class="register-photo">
                    <div class="form-container ml-0">
                        <div class="image-holder"></div>
                        <form action="../ServletVotanteModificacion" id="formRegistro">
                            <h2 class="text-center mb-3">
                                <!--<img src="../CSS/IMG/voto.png" width="40" height="40" class="d-inline-block align-top" alt="">-->
                                <strong>Editar perfil</strong>votante.
                            </h2>
                            <!-- NOMBRE, APELLIDOS -->
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="inputNombreVotante">Nombre</label>
                                    <input type="text" class="form-control " id="inputNombreVotante" name="nombreVotante" placeholder="Nombre" required value="<%=objVotante.getNombreVotante()%>">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputApellidoVotante">Apellidos</label>
                                    <input type="text" class="form-control " id="inputApellidoVotante" name="apellidoVotante" placeholder="Apellido" required value="<%=objVotante.getApellidoVotante()%>">
                                </div>
                            </div>
                            <!--NIF-->
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="inputNifVotante">Nif</label>
                                    <input type="text" class="form-control " id="inputNifVotante" name="nifVotante" placeholder="Nif" required readonly value="<%=objVotante.getNifVotante()%>">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputDomicilioVotante">Domicilio</label>
                                    <input type="text" class="form-control " id="inputDomicilioVotante" name="domicilioVotante" placeholder="Domicilio" required value="<%=objVotante.getDomicilioVotante()%>">
                                </div>
                            </div>
                            <!--PASSWORD-->
                            <div class="form-group row">
                                <label for="inputPassVotante" class="col-sm-4 col-form-label">Password</label>
                                <div class="col-sm-8">
                                    <input type="password" class="form-control" id="inputPassVotante" name="passVotante" placeholder="Password" required value="<%=objVotante.getPassVotante()%>">
                                </div>
                            </div>
                            <!--CONTENIDO-->
                            <div class="form-group row">
                                <label for="inputFechaNacVotante" class="col-sm-4 col-form-label">Fecha Nacimiento</label>
                                <div class="col-sm-8">
                                    <input type="date" class="form-control " id="inputFechaNacVotante" name="fechaNacVotante" required value="<%=objVotante.getFechaNacVotante()%>">
                                </div>
                            </div>

                            <input class="btn btn-outline-secondary " type="submit" id="botonEnviarBajaVotante" value="Editar Perfil">
                        </form>

                    </div>
                </div>
            </div>
        </section>
    </body>
</html>
