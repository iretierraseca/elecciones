<%-- 
    Document   : vistaAdministradorListadoCenso
    Created on : 22-nov-2018, 23:20:49
    Author     : Irene
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="packageElecciones.MODELO.votante"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="vistaMenuAdministrador.jsp"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <style>
            #inventario{
                height: 35%;
                overflow-y: scroll;
            }

            #filaDatosEst{
                background-color:#a98257 !important;
            }

            #seccionContainer .container{
                border: 2px solid #f5d5be;
                box-shadow: 2px 2px 5px #f5d5be;
            }
            
        </style>
    </head>
    <body>
        <%
            //HttpSession SesionUsuario = request.getSession(true);
            ArrayList<votante> arrayListCenso = (ArrayList<votante>) session.getAttribute("arrayUsuarios");
        %>
        <section id="seccionContainer" >
            <div class="container rounded mt-5 opaco" id="inventario">
                <h3 class="text-center">Listado Censo</h3>
                <table border="1" width="100%" class="mt-2 table table-bordered text-center">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Apellido</th>
                            <th>Nif</th>
                            <th>Domicilio</th>
                            <th>Votado</th>
                        </tr>
                    </thead>
                    <tbody>
                        <%
                        for (int i = 0; i < arrayListCenso.size(); i++) {
                        %>
                        <tr>
                            <td><%=arrayListCenso.get(i).getNombreVotante()%></td>
                            <td><%=arrayListCenso.get(i).getApellidoVotante()%></td>
                            <td><%=arrayListCenso.get(i).getNifVotante()%></td>
                            <td><%=arrayListCenso.get(i).getDomicilioVotante()%></td>
                            <td><%=arrayListCenso.get(i).getVotado()%></td>
                            
                            
                        </tr>
                        <%
                        }
                        %>
                    </tbody>

                </table>

            </div>
        </section>
    </body>
</html>
