<%-- 
    Document   : vistaMensajeError
    Created on : 24-nov-2018, 11:12:11
    Author     : Irene
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%String usuario = request.getParameter("tipoUsuario");%>
<jsp:include page="<%=usuario+".jsp"%>"/>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <section id="seccionContainer">
            <div class="container" id="ContainerSeccionError">
                <div class="mt-3 alert alert-danger" role="alert">
                    <%String error = request.getParameter("mensajeError");%>
                    <%out.println(error);%>
                </div>
            </div>
        </section>
    </body>
</html>