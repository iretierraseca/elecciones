<%-- 
    Document   : vistaAdministradorEscrutinio
    Created on : 08-dic-2018, 21:52:38
    Author     : Irene
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="vistaMenuAdministrador.jsp"%>
<%String mensaje = request.getParameter("mensajeEscrutinio");%>
<%String opcionEscrutinio = request.getParameter("opcionEscrutinio");%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <style>
            #containerBaja{
                border: 2px solid #ffeeba !important;
                box-shadow: 2px 2px 5px #ffeeba !important;
            }
        </style>
    </head>
    <body>

        <section id="seccionContainer">
            <div class="container" id="containerBaja">
                <form method="POST" action="../ServletAdministradorEscrutinio?opcionEscrutinio=<%=opcionEscrutinio%>">
                    <div class="alert alert-warning mt-3" role="alert">
                        <h4 class="alert-heading">Hola, <%=objVotante.getRol()%></h4>
                        <p><%out.print(mensaje);%></p>
                        <hr>
                        <p class="mb-0">
                            <input class="btn btn-primary " type="submit" id="botonEnviarBajaVotante" name="botonBaja" value="Aceptar">
                            <input class="btn btn-warning " type="submit" id="botonEnviarBajaVotante" name="botonBaja" value="Cancelar">
                        </p>
                    </div>
                </form>
            </div>
        </section>
    </body>
</html>
