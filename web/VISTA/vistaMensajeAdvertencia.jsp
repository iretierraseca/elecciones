<%-- 
    Document   : vistaMensajeAdvertencia
    Created on : 22-nov-2018, 17:34:10
    Author     : Irene
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="stylesheet" href="../CSS/style.css">
    </head>
    <body>
       <section id="seccionContainer">
            <div class="container" id="ContainerSeccionError">
                <div class="mt-3 alert alert-danger" role="alert">
                    <%String error = request.getParameter("mensajeError");%>
                    <%out.println(error);%>
                    <%response.setHeader("Refresh", "5;url=vistaLoginRegistro.jsp");%>
                </div>
            </div>
        </section>
    </body>
</html>
