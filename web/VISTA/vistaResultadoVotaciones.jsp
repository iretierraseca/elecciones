<%-- 
    Document   : vistaResultadoVotaciones
    Created on : 09-dic-2018, 16:38:32
    Author     : Irene
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="packageElecciones.MODELO.partido"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%String usuario = request.getParameter("tipoUsuario");%>
<jsp:include page="<%=usuario + ".jsp"%>"/>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.bundle.min.js"></script>
    </head>
    <body>
        <%
            //HttpSession SesionUsuario = request.getSession(true);
            ArrayList<partido> arrayListPartido = (ArrayList<partido>) session.getAttribute("arrayPartidos");
            ArrayList<String> arrayListSiglasPartido = new ArrayList<String>();
            ArrayList<Integer> arrayListVotosPartido = new ArrayList<Integer>();

            for (int i = 0; i < arrayListPartido.size(); i++) {
                arrayListSiglasPartido.add(arrayListPartido.get(i).getSiglasPartido());
                arrayListVotosPartido.add(arrayListPartido.get(i).getNumeroVotosPartido());
            }
        %>
        <section id="seccionContainer">
            <div class="container" id="ContainerSeccionError">
                <canvas id="myChart"></canvas>
            </div>
        </section>
        <script>
            var arraySiglasPartido = "<%=arrayListSiglasPartido%>";
            arraySiglasPartido = arraySiglasPartido.replace("[", "").replace("]", "");
            arraySiglasPartido = arraySiglasPartido.split(", ");

            var arrayVotosPartido = "<%=arrayListVotosPartido%>";
            arrayVotosPartido = arrayVotosPartido.replace("[", "").replace("]", "");
            arrayVotosPartido = arrayVotosPartido.split(", ");

            var ctx = document.getElementById("myChart").getContext('2d');
            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: arraySiglasPartido,
                    datasets: [{
                            label: 'Elecciones 2018',
                            data: arrayVotosPartido,
                            backgroundColor: ["#008cd7", "#e4010b", "#6a2c65", "#fc6b18"],
                            borderColor: "#3cba9f"
                        }]
                },
                options: {
                    title: {
                        display: true,
                        text: 'Resultados votaciones (Partido/Votos)'
                    }
                }
            });
        </script>
    </body>
</html>
